//
//  common.swift
//  Attestation
//
//  Created by pavel ye on 1/9/17.
//  Copyright © 2017 pavel ye. All rights reserved.
//

import Foundation

enum StrError : Error
{
	case description(String)
}
