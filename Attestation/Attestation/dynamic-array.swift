//
//  dynamic-array.swift
//  Attestation
//
//  Created by pavel ye on 1/9/17.
//  Copyright © 2017 pavel ye. All rights reserved.
//

import Foundation

// https://www.hackerrank.com/challenges/dynamic-array

let minSeqCount = 1
let maxSeqCount = 100000
let kMinX = 0
let kMaxX = 1000000000

func challengeDynamicArray() throws
{
	let parsedInput = try readInputData()
	let seqList = try SeqList(sequencesCount: parsedInput.sequencesCount)
	for q in parsedInput.queries
	{
		if q.qtype == .Append
		{
			try seqList.append(position: q.x, value: q.y)
		} else
		{
			try seqList.assign(position: q.x, value: q.y)
		}
	}
}

class SeqList
{
	required init(sequencesCount n : Int) throws
	{
		guard minSeqCount...maxSeqCount ~= n else
		{
			throw StrError.description("sequences count must be in range \(minSeqCount) .. \(maxSeqCount)")
		}
		
		self.n = n
		self.sequences = Array.init(repeating: [], count: n)
	}
	
	func append(position x: Int, value y : Int) throws
	{
		let ix = try seqIndex(x)
		self.sequences[ix].append(y)
	}
	
	func assign(position x: Int, value y : Int) throws
	{
		let ix = try seqIndex(x)
		let sequence = self.sequences[ix]
		let subIx = try subSeqIndex(sequence: sequence, y : y)
		self.lastAns = sequence[subIx]
		print (self.lastAns)
	}
	
	private func seqIndex(_ x: Int) throws -> Int
	{
		guard kMinX...kMaxX ~= x else
		{
			throw StrError.description("x must be in range \(kMinX) .. \(kMaxX)")
		}
		
		let ix = (x ^ lastAns) % self.n
		guard ix < self.sequences.count else
		{
			throw StrError.description("wrong x parameter \(x)")
		}
		return (x ^ lastAns) % self.n
	}
	
	private func subSeqIndex(sequence: Array<Int>, y: Int) throws -> Int
	{
		let size = sequence.count
		let ret = y % size
		guard ret < sequence.count else
		{
			throw StrError.description("wrong y parameter \(y)")
		}
		return ret
	}
	
	private(set) var n : Int
	private var sequences : Array< Array<Int> >
	private var lastAns = 0
}

private enum QueryType : Int
{
	case Append = 1
	case Assign = 2
}

private struct ParsedData
{
	struct Query
	{
		let x : Int
		let y : Int
		let qtype : QueryType
	}
	var sequencesCount : Int = 0
	var queries = [Query]()
}

private func readInputData() throws -> ParsedData
{
	guard let fistLine = readLine(), !fistLine.isEmpty else
	{
		throw StrError.description("Input has wrong format")
	}
	
	guard let sequencesCount = Int(fistLine.components(separatedBy: " ")[0]),
	let queriesCount = Int(fistLine.components(separatedBy: " ")[1])
	else
	{
		throw StrError.description("cannot parse 0-line, should be: [N] [Q]")
	}

	
	var parsed = ParsedData()
	parsed.sequencesCount = sequencesCount
	
	var index = 1
	while let line = readLine(), index <= queriesCount
	{
		
		let tokens = line.components(separatedBy: " ")
		guard tokens.count >= 3 else
		{
			throw StrError.description("Line[\(index)]: \(line) has wrong format")
		}
		guard let x = Int(tokens[1]), let y = Int(tokens[2]) else
		{
			throw StrError.description("Line[\(index)]: \(line) x,y did not parsed")
		}
		
		let qTypeStr = tokens[0]
		let qTypeNum = Int(qTypeStr)
		guard qTypeNum == 1 || qTypeNum == 2  else
		{
			throw StrError.description("Query type \(qTypeStr) is not supported")
		}
		
		let qtype : QueryType = (qTypeNum == 1) ? .Append : .Assign
		
		let q = ParsedData.Query(x: x, y: y, qtype: qtype)
		parsed.queries.append(q)
		index = index + 1
	}
	
	guard parsed.queries.count == queriesCount else
	{
		throw StrError.description("Queries count is wrong \(queriesCount) <> \(parsed.queries.count)")
	}
	
	return parsed
}


