//
//  main.swift
//  Attestation
//
//  Created by pavel ye on 1/3/17.
//  Copyright © 2017 pavel ye. All rights reserved.
//

import Foundation

/*
https://www.hackerrank.com/challenges/nlp-pos-tagging
https://www.hackerrank.com/challenges/ajourney
https://www.hackerrank.com/challenges/dynamic-array
*/

do
{
	print ("challenge 1: PosTagging")
	try challengePosTagging()
	print ("______________________\n")
	
	print ("challenge 2: Journey To Mars")
	let ajourney = Ajourney()
	try ajourney.challengeJourneyToMars()
	print ("______________________\n")
	
	print ("challenge 3: DynamicArray.")
	print ("Please, input your data...\n")
	try challengeDynamicArray()
	print ("______________________\n")
}
catch (let s)
{
	print (s)
}
