//
//  nlp-pos-tagging.swift
//  Attestation
//
//  Created by pavel ye on 1/9/17.
//  Copyright © 2017 pavel ye. All rights reserved.
//

import Foundation

// https://www.hackerrank.com/challenges/nlp-pos-tagging

let inputText = "The/DT planet/NN Jupiter/NNP and/CC its/PPS moons/NNS are/VBP in/IN effect/NN a/DT minisolar/JJ system/?? ,/, and/CC Jupiter/NNP itself/PRP is/VBZ often/RB called/VBN a/DT star/?? that/IN never/RB caught/??? fire/NN ./."


func challengePosTagging() throws
{
	print(inputText)
	print("")
	var storage = TagStorage()
	var placeHolderRanges = [NSRange]()
	
	do {
		let regex = try NSRegularExpression(pattern: "\\/([A-Z\\?]+?)\\s")
		let matches = regex.matches(in: inputText, options: [], range: NSRange(location: 0, length: inputText.characters.count))
		
		let s = inputText as NSString
		for match in matches
		{
			guard match.numberOfRanges == 2 else
			{
				return
			}
			let r = match.rangeAt(match.numberOfRanges - 1)
			let tag = s.substring(with: r)
			
			if isPlaceHolder(tag)
			{
				placeHolderRanges.append(r)
			} else
			{
				storage.add(tag)
			}
		}
	} catch let error
	{
		throw StrError.description("invalid regex: \(error.localizedDescription)")
	}
	

	var resultNSStr = inputText as NSString
	for r in placeHolderRanges
	{
		
		if let tag = storage.find(r.length)
		{
			resultNSStr = resultNSStr.replacingCharacters(in: r, with: tag) as NSString
		} else
		{
			throw StrError.description("Cannot find tag for \(r.length) letters")
		}
	}
	
	print (resultNSStr)
	
}

private struct TagStorage
{
	mutating func add(_ tag : String)
	{
		guard isValidTag(tag) else
		{
			return
		}
		let cnt = tag.characters.count
		if dict[cnt] == nil
		{
			dict[cnt] = []
		}
		let _ = dict[cnt]?.update(with: tag)
	}
	
	func find(_ lettersCount : Int) -> String?
	{
		/* Fixme: it is not specified by the task
		*  how to choose the only tag from a set. So just return the first
		*  Also I know, "/NN", for example means "noun", "VBP" means - verb etc
		*  But the task does not require that tags should be grammar correct
		*/
		return dict[lettersCount]?.first
	}
	
	private var dict = Dictionary<Int, Set<String> >()
}

private func isPlaceHolder(_ test : String) -> Bool
{
	guard !test.isEmpty else
	{
		return false
	}
	let chSet = NSCharacterSet.init(charactersIn: "?").inverted
	return test.rangeOfCharacter(from: chSet) == nil
}

private func isValidTag(_ test : String) -> Bool
{
	guard !test.isEmpty else
	{
		return false
	}
	let chSet = NSCharacterSet.uppercaseLetters.inverted
	return test.rangeOfCharacter(from: chSet) == nil
}
