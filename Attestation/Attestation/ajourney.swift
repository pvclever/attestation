//
//  ajourney.swift
//  Attestation
//
//  Created by pavel ye on 1/9/17.
//  Copyright © 2017 pavel ye. All rights reserved.
//

import Foundation

// https://www.hackerrank.com/challenges/ajourney

// https://brilliant.org/wiki/modular-arithmetic
// https://en.wikipedia.org/wiki/Fermat's_little_theorem
// https://www.mtholyoke.edu/courses/quenell/s2003/ma139/js/powermod.html

class Ajourney
{
	
	let minN = 1
	let maxN = 1000000000 // maximum staircase's steps
	let minK = 1
	let maxK = 9 // max digits count
	let minT = 1
	let maxT = 1000 // max test cases count
	
	func challengeJourneyToMars() throws
	{
		let inputData = try readInputData()
		for test in inputData
		{
			// ways count = 2^(n-1)
		
			let pover = test.stepsCount - 1
			let firstDigits = getFirstDigits(n: 2, pover: pover, digits: test.digitsCount)
			let lastDigits = getLastDigits(pover: pover, digits: test.digitsCount)
			print (firstDigits + lastDigits)
		}
		return
	}

	private func readInputData() throws -> [TestCase]
	{
		
		guard let str = readLine(), let testCount = Int(str),
			testCount >= minT, testCount <= maxT else
		{
			throw StrError.description("Wrong tests count T, should be in range \(minT) \(maxK)")
		}
		
		var data = [TestCase]()
		var cnt = 0
		while let line = readLine(), cnt < testCount
		{
			if line.isEmpty
			{
				throw StrError.description("Wrong format: empty line")
			}
			
			let tokens = line.components(separatedBy:(" "))
			guard tokens.count >= 2 else
			{
				throw StrError.description("Wrong format for case: \(line)")
			}
			let stepsCountStr = tokens[0]
			let digitsCountStr = tokens[1]
			
			
			guard let stepsCount = Int(stepsCountStr), let digitsCount = Int(digitsCountStr),
				stepsCount > 0, digitsCount > 0 else
			{
				throw StrError.description("Wrong input line \(line)")
			}
			
			guard minN ... maxN ~= stepsCount else
			{
				throw StrError.description("steps N should be in range \(minN) \(maxN)")
			}
			
			guard minK ... maxK ~= digitsCount else
			{
				throw StrError.description("digits count N should be in range \(minK) \(maxK)")
			}
			
			let testCase = TestCase(stepsCount: stepsCount, digitsCount: digitsCount)
			data.append(testCase)
			
			cnt = cnt + 1
		}
		
		return data
	}
	
	private struct TestCase
	{
		var stepsCount : Int
		var digitsCount : Int
	}

	private func getLastDigits(pover : Int, digits : Int) -> Int
	{
		assert(digits > 0)

		let modular = Int(pow(Double(10), Double(digits)))
		let remainer = powWithMod(a: 2, power: pover, modular: modular)
			
			//getRemainer(n: n, pover: pover, modular: Int(modular))
		
		return remainer
	}
	
	/* calculates (a^power) % modular */
	private func powWithMod(a : Int, power : Int, modular : Int) -> Int
	{
		var x = 1
		var y = a
		var b = power
		
		while b > 0
		{
			if b % 2 == 1
			{
				x = (x * y)
				if x > modular
				{
					x %= modular;
				}
			}
			
			y = y * y
			if y > modular
			{
				y %= modular
			}
			
			b /= 2
		}
		return x
	}
	
	let myLog2 : Float80 = 0.3010299956639811952137388947244930267681898814621085413104274611271081892744245094869272521181861720406844771914309953
	
	// use for checking:
	// http://www.ttmath.org/online_calculator
	private func getFirstDigits(n: Int, pover : Int, digits : Int) -> Int64
	{
		let logFactor = myLog2 * Float80(pover)
		let fractionalPart = logFactor - floor(logFactor)
		let result = floor(pow(10, Double(fractionalPart + Float80(digits - 1))))
		return Int64(result)
	}
	
}
